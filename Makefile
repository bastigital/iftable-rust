# A Makefile to build, test, tag and publish the Emscripten SDK Docker container.

# Emscripten version to build: Should match the version that has been already released.
# i.e.:  1.39.18
mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
tool_dir := $(dir $(mkfile_path))

serial?=/dev/ttyUSB0
baud?=57600
target?=release
outfile=${tool_dir}/target/xtensa-esp32-none-elf/${target}/firmware
flash_mode=dio

.TEST:
ifeq (,$(wildcard ${outfile}))
    $(error binary ${outfile} does not exist)
endif

.PHONY: flash
flash: elf2bin esptool-flash

.PHONY: elf2bin
elf2bin: .TEST
	esptool.py --chip esp32 elf2image --flash_mode ${flash_mode} --flash_freq 40m --flash_size 4MB ${outfile}

.PHONY: esptool-flash
esptool-flash: .TEST
	esptool.py --chip esp32 -p ${serial} -b ${baud} --before=default_reset \
		--after=hard_reset write_flash --flash_mode ${flash_mode} --flash_freq 40m \
		--flash_size 4MB 0x8000 bin/partition-table.bin 0x1000 bin/bootloader.bin \
		0x10000 ${outfile}.bin