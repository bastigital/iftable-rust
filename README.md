# Iftable Rust

This is a playground project to test the rust implementation for esp32.

## Run dev container

Since we needed to build a custom rustc to support xtensa targets and this takes a lot of time, it is recommended to use the VS Code Remote Container provided in this repository.

1. Get the extension and install it in VS Code: https://github.com/Microsoft/vscode-remote-release
1. Ctrl+Shift+P -> Open Folder in Container -> select this repository

This should run a VS Code instance in the prebuilt docker container.

---
**Note**

This will download approx 20GB of data !

---

## Build

$ cargo xbuild

## Flash

$ cargo espflash --release --tool=xbuild --chip=esp32 /dev/ttyUSB0

## Links:

- Rust-Xtensa: https://github.com/MabezDev/rust-xtensa
- esp-rs: https://github.com/esp-rs