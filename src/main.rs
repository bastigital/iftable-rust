#![no_std]
#![no_main]
#![feature(alloc_error_handler)]

extern crate iftable;
extern crate xtensa_lx_rt;
use core::{fmt::Write, panic::PanicInfo};
use cstr_core::CStr;
use xtensa_lx_rt::entry;

use esp32_hal::{
    clock_control::{sleep, ClockControl, ClockControlConfig, XTAL_FREQUENCY_AUTO},
    dport::Split,
    dprintln,
    prelude::*,
    serial::{config::Config, Pins, Serial},
    target,
    timer::Timer,
};

use esp32_hal::alloc::{Allocator, DEFAULT_ALLOCATOR};

#[global_allocator]
pub static GLOBAL_ALLOCATOR: Allocator = DEFAULT_ALLOCATOR;

#[alloc_error_handler]
fn alloc_error_handler(layout: core::alloc::Layout) -> ! {
    panic!(
        "Error allocating  {} bytes of memory with alignment {}",
        layout.size(),
        layout.align()
    );
}

#[entry]
fn main() -> ! {
    sleep(5.s());
    let dp = target::Peripherals::take().expect("Failed to obtain Peripherals");

    let (_, dport_clock_control) = dp.DPORT.split();

    let clkcntrl = ClockControl::new(
        dp.RTCCNTL,
        dp.APB_CTRL,
        dport_clock_control,
        XTAL_FREQUENCY_AUTO,
    )
    .unwrap();

    let (clkcntrl_config, mut watchdog) = clkcntrl.freeze().unwrap();
    watchdog.disable();

    let (_, _, wifi_timer, mut watchdog0) = Timer::new(dp.TIMG0, clkcntrl_config);
    let (_, _, _, mut watchdog1) = Timer::new(dp.TIMG1, clkcntrl_config);
    watchdog0.disable();
    watchdog1.disable();

    let pins = dp.GPIO.split();

    let mut serial: Serial<_, _, _> = Serial::new(
        dp.UART0,
        Pins {
            tx: pins.gpio1,
            rx: pins.gpio3,
            cts: None,
            rts: None,
        },
        Config {
            // default configuration is 19200 baud, 8 data bits, 1 stop bit & no parity (8N1)
            baudrate: 115200.Hz(),
            ..Config::default()
        },
        clkcntrl_config,
    )
    .unwrap();

    let (mut tx, mut rx) = serial.split();

    writeln!(tx, "\n\nESP32 Started\n\n").unwrap();

    loop {
        sleep(1.s());
        watchdog.feed();
        watchdog0.feed();
        watchdog1.feed();
    }
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    // park the other core
    unsafe { ClockControlConfig {}.park_core(esp32_hal::get_other_core()) };

    // print panic message
    dprintln!("\n\n*** Core: {:?} {:?}", esp32_hal::get_core(), info);

    // park this core
    unsafe { ClockControlConfig {}.park_core(esp32_hal::get_core()) };

    dprintln!("\n\n Should not reached because core is parked!!!");

    // this statement will not be reached, but is needed to make this a diverging function
    loop {}
}
